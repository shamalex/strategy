<?php

/*========================================================
 *   @file:          contacts.php
 *   @description:   Обработка формы для сайта Стратегия
 *   @author:        Alex Shamshin
 *   @contact:       shamshin.alexandr@gmail.com
 *   @date:          18.12.2016
=========================================================*/

if(isset($_POST['tel'])){

    $name = $_POST['name'];
    $tel = $_POST['tel'];
    $dolg = $_POST['dolg'];


    $to      = 'shamshin.alexandr@gmail.com';
    $subject = 'СТРАТЕГИЯ - форма заявки';

    $headers = 'Phone: '. $tel . "\r\n" .
    'Reply-To: '. $tel . "\r\n" .
    'select_dolg: '. $dolg . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    $status = mail($to, $subject, $headers);

    if($status == TRUE){
        $res['sendstatus'] = 'done';

        //Edit your message here
        $res['message'] = '<span style="color:green">Заявка успешно отправлена!</span>';
    }
    else{
        $res['message'] = 'Не удается отправить почту, свяжитесь по USENAME@gmail.com';
    }
    echo json_encode($res);
}

?>

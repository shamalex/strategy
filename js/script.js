/*========================================================
 *   @file:          script.js
 *   @description:   глобальные скрипты для сайта Стратегия
 *   @author:        Alex Shamshin
 *   @contact:       shamshin.alexandr@gmail.com
 *   @date:          18.12.2016
=========================================================*/


$(document).ready(function() {

    /* ---------------------------------------------- /*
     * init slider
    /* ---------------------------------------------- */
    $('.slick').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        infinite: true,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
        ]
      });

    $(".menu a[href*=#]").bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top-100
        }, 500);
        e.preventDefault();
        return false;
    });

    /* ---------------------------------------------- /*
     * init style for selects
    /* ---------------------------------------------- */
    $('.name, select').styler();

    /* ---------------------------------------------- /*
     * init tabs
    /* ---------------------------------------------- */
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

    /* ---------------------------------------------- /*
     * accordion init for faq
    /* ---------------------------------------------- */
    $(".inner").hide();
    $(".inner:first").show(); // THIS LINE IS ADDED!!!
    $(".accordion a").click(function(event){
        event.preventDefault();
        if ($(this).is(".current")) {
            $(this).removeClass("current");
            $(this).next(".inner").removeClass('open').slideUp(400);
        }
        else {
            $(".inner").slideUp(400);
            $(".accordion a").removeClass("current");
            $(this).addClass("current");
            $(this).next(".inner").addClass('open').slideDown(400);
        }
    });

    /* ---------------------------------------------- /*
     * switch inputs init
    /* ---------------------------------------------- */
    $('.switch').click(function(){
        $(this).toggleClass("switchOn");
    });

    /* ---------------------------------------------- /*
     * stiky header
    /* ---------------------------------------------- */

    if (document.body.clientWidth>800){
       $(".header").stick_in_parent();
        $(".tabs-wrap").stick_in_parent({
            offset_top: 100
        });
    }

    /* ---------------------------------------------- /*
     * logic questions
    /* ---------------------------------------------- */

    $('#quest2').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('.question1').hide().addClass('animated zoomOut');//скрываем первый вопрос
        $('.question2').show().addClass('animated zoomIn');//показываем второй вопрос
        if($("#radio1").is(':checked')) {
            $('.question2').hide().addClass('animated zoomOut');//прячем второй вопрос
            $('#alternative').show().addClass('animated zoomIn');//Показываем альтернативную ликвидацию
        }
    });

    $('#quest_result').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('.question2').hide();//прячем второй вопрос

        if($("#radio2").is(':checked')) {
            $('#liquidation').show().addClass('animated zoomIn');//Показываем ликвидацию
        } else if($("#radio3").is(':checked')) {
            $('#alternative').show().addClass('animated zoomIn');//Показываем альтернативную ликвидацию
        } else if($("#radio4").is(':checked')) {
            $('#bankrotstvo').show().addClass('animated zoomIn');//Показываем банкротство
        }
    });

    /* ---------------------------------------------- /*
     * WOW Animation When You Scroll
    /* ---------------------------------------------- */

    wow = new WOW({
        mobile: false
    });
    wow.init();

    /* ---------------------------------------------- /*
     * Order form ajax
    /* ---------------------------------------------- */

        $('#call, #call_ur, #call_close, #call_free, #call_free_ur, #call_free_ser').submit(function(e) {

            e.preventDefault();

            var c_name = $(this).find('.name').val();
            var c_phone = $(this).find('.tel').val();
            var response = $(this).find('.ajax');

            var formData = {
                'name'       : c_name,
                'tel'      : c_phone,
            };


            if ( c_name== '' || c_phone == '' ) {
                response.fadeIn(500);
                response.html('<span style="color:red">Заполните все поля!</span>');
            }

            else {
                     $.ajax({
                            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                            url         : 'php/contact.php', // the url where we want to POST
                            data        : formData, // our data object
                            dataType    : 'json', // what type of data do we expect back from the server
                            encode      : true,
                            success     : function(res){
                                            var ret = $.parseJSON(JSON.stringify(res));
                                            response.html(ret.message).fadeIn(500);
                                            $('body').find('.spasibo').show('slow');
                            }
                        });
                }
                return false;
        });
        $('#call_calc').submit(function(e) {

            e.preventDefault();
            var c_phone = $(this).find('.tel').val();
            var c_dolg = $('#dolg').val();
            var response = $(this).find('.ajax');

            var formData = {
                'tel'      : c_phone,
                'dolg'      : c_dolg,
            };


            if (c_phone == '' || c_dolg == '') {
                response.fadeIn(500);
                response.html('<span style="color:red">Заполните все поля!</span>');
            }

            else {
                     $.ajax({
                            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                            url         : 'php/contact.php', // the url where we want to POST
                            data        : formData, // our data object
                            dataType    : 'json', // what type of data do we expect back from the server
                            encode      : true,
                            success     : function(res){
                                            var ret = $.parseJSON(JSON.stringify(res));
                                            response.html(ret.message).fadeIn(500);
                                            $('body').find('.spasibo').show('slow');
                            }
                        });
                }
                return false;
        });

        $('.more a').on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $('.accordion li').removeClass('hidden');
            $(this).hide();
        });
});

    /* ---------------------------------------------- /*
     * clear all form on close modals
    /* ---------------------------------------------- */
    $(document).on('closed', '.remodal', function (e) {
        $("form").trigger('reset');
        $('.ajax').html('');
    });
